export interface Task{
  title: string;
  closed: boolean;
}
