# data-flux-playground

## Deploy on Netlify
```
https://advanced-dev-bellanger-jeremie.netlify.app/#/
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Vscode configuration
```
  We need eslint, vetur, prettier extensions.

  We must add in setting.json:

  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "editor.formatOnSave": true,
  "vetur.validation.template": true,
```
### Lint
```
  Execute on precommit with husky
  Lint on save
```

### Visual testing

```
  Execute on prepush with husky
  See printScreen of percy in assets/image
```
