import { Selector, t } from 'testcafe'

class Todolist {
  constructor () {
    this.todoFormInput = Selector('#todo-form-input')
    this.todoFormValid = Selector('#todo-form-btn')
    this.todoItem = Selector('.item > button')
  }

  async setTask (text) {
    await t
      .typeText(this.todoFormInput, text)
      .click(this.todoFormValid)
  }

  async setStatusTask () {
    await t.click(this.todoItem)
  }
}
export default new Todolist()
