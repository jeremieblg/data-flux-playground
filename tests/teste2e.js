import todolist from './page-models/todoList'
import percySnapshot from '@percy/testcafe'
const URL = 'http://localhost:8080/'

fixture('TodoList').page(URL)

const snap = async (t, name) => {
  await t.wait(200)
  await percySnapshot(t, name)
}

test('Add 3 task', async t => {
  await todolist.setTask('Tintin')
  await todolist.setTask('Milou')
  await todolist.setTask('Haddock')
  await snap(t, 'Add 3 task')
})

test('Change status of task', async t => {
  await todolist.setTask('Tintin')
  await snap(t, 'Add task')
  await todolist.setStatusTask()
  await snap(t, 'Change status of task')
})
